# DiUS Checkout

This implementation of "checkout" in Ruby centers around the Checkout object, which is documented in *lib/checkout.rb*

Give it a whirl with:
```
ruby run.rb [sku1] [sku2] ...
```
## Checkout

See Checkout class-level documentation in comments

- model/cart describes the cart object with array of skus

- model/catalogue describes the catalogue object with scanned products.json

## Specials

The majority of logic for totalling the cart lies in Specials, which can be defined in:
```
specials.json
```
Three broad categories of "specials" are represented as objects and were identified as:
- 3-for-2 (specify with a list of **sku**s)
- bulk-discount (specify with list of **sku**, bulk-discount **limit** and discounted **price**)
- gift (specify with list of **buy**-item and **gift**-item)

Specials were developed in BDD-style feature files at:

```
features/specials.feature
```

Initially, the rules are based on the requirements

- we're going to have a 3 for 2 deal on Apple TVs. For example, if you buy 3 Apple TVs, you will pay the price of 2 only

- the brand new Super iPad will have a bulk discounted applied, where the price will drop to $499.99 each, if someone buys more than 4

- we will bundle in a free VGA adapter free of charge with every MacBook Pro sold

The step definitions for these features have been implemented at:
```
features/step_definitions/specials_spec.rb
```
These step definitions can be improved with additional test cases with modularity of specific items and prices - perhaps using the product catalogue...

## Product Catalogue

```
products.json
```

| SKU     | Name        | Price    |
| --------|:-----------:| --------:|
| ipd     | Super iPad  | $549.99  |
| mbp     | MacBook Pro | $1399.99 |
| atv     | Apple TV    | $109.50  |
| vga     | VGA adapter | $30.00   |


## DiUS Interface

```java
  Checkout co = new Checkout(pricingRules);
  co.scan(item1);
  co.scan(item2);
  co.total();
```

## Test Cases

Cucumber features, run with:
```
rake features
```

Improvements TODO

- The test cases were written with broad testability in mind. Time limit led to use of fixed scenarios.
This can be improved with use of products.json and using prices, names, skus in testing

- The catalogue class was written in a hurry, it can be better used (currently loaded at both checkout and specials)

- The structure of specials.json was a cause of pain during development, it could be improved to reduce specials computation

- The above subsequently led to ABC complexity on discount calculation

- Handling of any skus apart from those listed in products.json

## Against the rules

Used 'json' gem, oops!
