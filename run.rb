$LOAD_PATH << 'lib'
require 'checkout'
require 'specials'

# Load specials
specials = File.read('specials.json')
@checkout = Checkout.new(specials)
# Scan items
ARGV.each do |i|
  @checkout.scan(i)
end
# Output total
@checkout.calculate_total
puts 'Total = ' + @checkout.current_total.to_s
