#Specials Feature File
Feature: Opening Specials

  Scenario: 3-for-2 | we're going to have a 3 for 2 deal on Apple TVs. For example, if you buy 3 Apple TVs, you will pay the price of 2 only
    Given the cart contains 2 of "atv"
    When a third "atv" is also in the cart
    Then the total should be calculated where the third item is free

  Scenario: Bulk discounts | the brand new Super iPad will have a bulk discounted applied, where the price will drop to $499.99 each, if someone buys more than 4
    Given the cart contains 5 of "ipd"
    When calculating the total price of those items
    Then those items should be totalled at a new price of $2499.95

  Scenario: Free gift with another purchase | we will bundle in a free VGA adapter free of charge with every MacBook Pro sold
    Given there exists a "mbp" in the cart
    When a "vga" is also in the cart
    Then the total should be calculated at $1399.99
