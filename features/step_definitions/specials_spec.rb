require 'specials'
require 'checkout'
require 'pry'

Before do
  specials = File.read('specials.json')
  @checkout = Checkout.new(specials)
end

Given(/^the cart contains (\d) of "(.*)"$/) do |count, sku|
  count.to_i.times { @checkout.cart.items.push(sku) }
end
When(/^a third "(.*)" is also in the cart$/) do |sku|
  @checkout.cart.items.push(sku)
end
Then(/^the total should be calculated where the third item is free$/) do
  expect(@checkout.calculate_total).to be == 219 # sorry, fix this for modularity!
end

Given(/^the cart contains more than (\d) of "(.*)"$/) do |count, sku|
  count.to_i.times { @checkout.cart.items.push(sku) }
  @checkout.cart.items.push(sku)
end
When(/^calculating the total price of those items$/) do
  @current_total = @checkout.calculate_total
end
Then(/^those items should be totalled at a new price of \$(.*)$/) do |price_string|
  expect(@current_total).to be == price_string.to_f
end

Given(/^there exists a "(.*)" in the cart$/) do |sku|
  @checkout.cart.items.push(sku)
end
When(/^a "(.*)" is also in the cart$/) do |sku|
  @checkout.cart.items.push(sku)
end
Then(/^the total should be calculated at \$(.*)/) do |price_string|
  expect(@checkout.calculate_total).to be == price_string.to_f
end
