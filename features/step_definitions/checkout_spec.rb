require 'cucumber'
require 'checkout'

Before do
  specials = File.read('specials.json')
  @checkout = Checkout.new(specials)
end

Given(/^the "ipd" sku is scanned$/) do
  @sku = 'ipd'
end
When(/^processing the sku in the checkout process$/) do
  @checkout.scan(@sku)
end
Then(/^the item is identified as "(.*)"$/) do |sku|
  expect(@checkout.cart.items[0]).to eq(sku)
end

Given(/^there are items in the cart$/) do
  @checkout.scan('mbp')
end
When(/^calculating the total of the items in the cart$/) do
  @current_total = @checkout.calculate_total
end
Then(/^a total value is returned$/) do
  expect(@current_total).to be > 0
end

Given(/^there is ipd,mbp,vga in the cart$/) do
  @checkout.cart.items = %w[ipd mbp vga]
end
When(/^summing the prices of each item$/) do
  @sum = @checkout.base_price(@checkout.cart.items)
end
Then(/^the sum of those three items is \$1979.98$/) do
  expect(@sum).to be == 1979.98
end
