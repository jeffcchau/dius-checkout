#Checkout Feature File
Feature: Have a functional checkout

  Scenario: Can scan an sku and add to a cart
    Given the "ipd" sku is scanned
    When processing the sku in the checkout process
    Then the item is identified as "ipd"

  Scenario: Can calculate the total on the current cart
    Given there are items in the cart
    When calculating the total of the items in the cart
    Then a total value is returned

  Scenario: Can calculate the sum of three items in the cart
    Given there is ipd,mbp,vga in the cart
    When summing the prices of each item
    Then the sum of those three items is $1979.98
