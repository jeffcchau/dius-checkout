require 'model/cart'
require 'model/catalogue'

# Checkout class defining the cart and the total
# The store catalogue and specials are preloaded
# Total price of checkout is calculated as base price of items minus any discounts
class Checkout
  attr_reader :cart
  attr_reader :current_total

  def initialize(specials)
    @current_total = 0
    @catalogue = Catalogue.new
    @specials = Specials.new(JSON.parse(specials), @catalogue)
    @cart = Cart.new
  end

  def scan(sku)
    @cart.add_item(sku)
  end

  def calculate_total
    # sum item prices
    base = base_price(@cart.items)
    # apply discounts
    discounts = @specials.discounts(@cart.items)
    # amend current total
    @current_total = base - discounts
  end

  def base_price(cart_item_skus)
    base_prices = []
    cart_item_skus.length.times do |i|
      base_prices[i] = @catalogue.find_by_sku(cart_item_skus[i])['price']
    end
    # sum each of the base prices in the array to get total base price
    base_prices.inject(0) { |total, price| total + price.to_f }
  end
end
