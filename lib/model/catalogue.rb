require 'json'

# Contains methods relevant to the catalogue
class Catalogue
  attr_reader :catalogue

  def initialize
    @catalogue = JSON.parse(File.read('products.json'))
  end

  def find_by_sku(sku)
    @catalogue['products'].find { |item| item['sku'] == sku }
  end
end
