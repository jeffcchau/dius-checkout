# Cart class defining the currently scanned items
class Cart
  # accessor for testing purposes, otherwise should use read
  attr_accessor :items

  def initialize
    @items = []
  end

  def add_item(sku)
    @items.push(sku)
  end
end
