require 'checkout'
require 'pry'

# Specials contains logic for applying discounts to a given list of skus
class Specials
  attr_reader :discount

  def initialize(specials, catalogue)
    @specials = specials
    @catalogue = catalogue
  end

  def discounts(skus)
    three_for_two_discount(skus) + bulk_discount(skus) + gift_discount(skus)
  end

  def three_for_two_discount(skus)
    # 3-for-2 special
    discount = 0
    skus.uniq.each do |sku|
      if @specials['3-for-2'].include? sku
        discount = (skus.count(sku) / 3) * @catalogue.find_by_sku(sku)['price'].to_f
      end
    end
    discount
  end

  def bulk_discount(skus)
    # Bulk discounts
    discount = 0
    skus.uniq.each do |sku|
      @specials['bulk-discount'].length.times do |i|
        if (@specials['bulk-discount'][i]['items'].include? sku) && (skus.count(sku) > @specials['bulk-discount'][i]['limit'])
          discount = skus.count(sku) * @specials['bulk-discount'][i]['discount'].to_f
        end
      end
    end
    discount
  end

  def gift_discount(skus)
    discount = 0
    @specials['gift'].length.times do |i|
      # if there is at least one matching "buy" item and one "get" item in the cart
      count_of_buys = skus.count(@specials['gift'][i]['buy'])
      count_of_frees = skus.count(@specials['gift'][i]['get'])
      if (count_of_buys > 0) && (count_of_frees > 0)
        # discount = min of count(buy) and count(get), times the price per "get"
        discount = [count_of_buys, count_of_frees].min * @catalogue.find_by_sku(@specials['gift'][i]['get'])['price'].to_f
      end
    end
    discount
  end
end
